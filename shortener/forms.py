from django import forms

from .models import Url

class UrlForm(forms.ModelForm):

    class Meta:
        model = Url
        fields = ('typed_url',)