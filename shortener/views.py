from django.shortcuts import render, redirect
from .models import Url

from .forms import UrlForm

def new_url(request):
    form = UrlForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('rank_urls')
    return render(request, 'new_url.html', {'form': form})

def rank_urls(request):
    urls = Url.objects.order_by('-clicks')[:5]
    return render(request, 'rank_urls.html', {'urls': urls})

def redirect_url(request, shortened_url):
    url = Url.objects.get(shortened_url=shortened_url)
    url.click()
    return redirect(url.typed_url)