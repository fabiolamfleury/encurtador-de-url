from django.test import TestCase
from django.db import IntegrityError
from http import HTTPStatus

from shortener.models import Url
from shortener.forms import UrlForm

TYPED_URL = "https://www.google.com/"

class UrlModelTestCase(TestCase):

    def test_url_is_shortened(self):
        """Url when created gains shortened attribute and starts with no clicks"""
        url = Url.objects.create(typed_url=TYPED_URL)
        self.assertTrue(TYPED_URL != url.shortened_url)
        self.assertEqual(url.clicks, 0)

    def test_url_is_repeated(self):
        """
        When trying to create a new Url object with an already created typed url
            it should raise IntegrityError
        """
        Url.objects.create(typed_url=TYPED_URL)
        with self.assertRaises(IntegrityError):
           Url.objects.create(typed_url=TYPED_URL)

    def test_url_is_clicked(self):
        url = Url.objects.create(typed_url=TYPED_URL)
        self.assertEqual(url.clicks, 0)
        url.click()
        self.assertEqual(url.clicks, 1)


class UrlFormTestCase(TestCase):
    def test_url_is_correct(self):
        """Url when valid shouldn't raise any errors"""
        form = UrlForm(data={"typed_url": TYPED_URL})
        self.assertEqual(form.errors, {})

    def test_url_is_not_valid(self):
        form = UrlForm(data={"typed_url": "url"})
        self.assertEqual(
            form.errors["typed_url"], ["Enter a valid URL."]
        )

    def test_url_is_repeated(self):
        """Url when created gains shortened attribute and starts with no clicks"""
        Url.objects.create(typed_url=TYPED_URL)
        form = UrlForm(data={"typed_url": TYPED_URL})
        self.assertEqual(
            form.errors["typed_url"], ["Url with this Typed url already exists."]
        )


class UrlViewTestCase(TestCase):
    def test_get_new_url(self):
        response = self.client.get("/")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "<h1>Encurtar link</h1>", html=True)

    def test_post_success_new_url(self):
        response = self.client.post(
            "/", data={"typed_url": TYPED_URL}
        )

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response["Location"], "/rank/")
    
    def test_post_error_new_url(self):
        response = self.client.post(
            "/", data={"typed_url": "url"}
        )

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "Enter a valid URL.", html=True)

    def test_get_rank(self):
        response = self.client.get("/rank/")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "<h1>Ranking de Urls mais clicadas</h1>", html=True)

    def test_get_url_redirect(self):
        url = Url.objects.create(typed_url=TYPED_URL)
        response = self.client.get(f"/r/{url.shortened_url}/")
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response["Location"], url.typed_url)