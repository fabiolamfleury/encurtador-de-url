from django.db import models
import os
from hashlib import md5


class Url(models.Model):
    typed_url = models.URLField(max_length=200, unique=True)
    shortened_url = models.URLField(max_length=100, unique=True)
    clicks = models.IntegerField(default=0)
    
    def save(self, *args, **kwargs):        
        self.shortened_url = md5(self.typed_url.encode()).hexdigest()[:8]

        return super().save(*args, **kwargs)

    def click(self):
        self.clicks = self.clicks + 1
        self.save()