
from django.contrib import admin
from django.urls import path
from shortener.views import new_url, rank_urls, redirect_url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', new_url, name='new_url'),
    path('rank/', rank_urls, name='rank_urls'),
    path('r/<str:shortened_url>/', redirect_url ,name='redirect'),
]
