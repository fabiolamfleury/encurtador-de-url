# Encurtador de Urls

A aplicação foi implementada usando o framework Django. Foi criado o app _shortener_ que contém a model Url, formada por a Url digitada pelo usuário e a Url encurtada pela aplicação, além da contagem de acessoas a url encurtada. Possui também forms, templates, views e testes, que estão de acordo com a arquitetura MVT proposta para o django. Toda a responsabilidade de encurtamento de Urls é feita pela model.

As rotas disponíveis são:

* localhost:8000/   - formulário para encurtamento de urls
* localhost:8000/rank   - ranking que apresenta as 5 urls mais clicadas
* localhost:8000/admin  - permite a visualização de administrador padrão do django, necessário o cadastro de uma conta de administrador.

Além dessas duas rotas, existem as rotas de encurtamento que tem o início /r/ seguido de uma hash que identifica a url, redirecionando-a.

## Pré-requisitos

Para conseguir levantar essa aplicação é necessário a instalação de:

- Docker-compose
- Docker

## Como usar

Para começar a usar a aplicação, rode o seguinte comando:

```
$ docker-compose up --build
```

Das próximas vezes que quiser rodar o projeto, não é necessária a flag --build

Para o banco de dados estar com as informações atualizadas da aplicação deve-se realizar as migrações, com o seguinte comando.

```
$ sudo docker-compose exec web python manage.py migrate
```


## Como rodar os testes

Para rodar os testes, certifique-se que o ambiente já está rodando e utilize o seguinte comando, em outro terminal:

```
$ docker-compose exec web python manage.py test
```

# Tecnologias Utilizadas

Além do python/django, foi utilizada a biblioteca materialize, para estilização, com arquivos css e js que constam como arquivos estáticos (shortener/static/).
